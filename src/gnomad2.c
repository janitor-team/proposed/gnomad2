/*
   gnomad2.c
   Main loop and GTK+ GUI setup file and callback functions
   Copyright (C) 2001-2011 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

*/

#include "common.h"
#include "jukebox.h"
#include "xfer.h"
#include "data.h"
#include "playlists.h"
#include "prefs.h"
#include "util.h"
#ifdef HAVE_GUDEV
#define G_UDEV_API_IS_SUBJECT_TO_CHANGE
#include <gudev/gudev.h>
const char * const gudev_subsystems[] = { "usb", NULL };
GUdevClient *gudev_client;
guint uevent_id;
guint uevent_bus_hooked = 0;
guint uevent_device_hooked = 0;
#endif

/* This one should be global really */
GtkWidget *main_window;

/* Local variables */
static GdkPixbuf *icon_pixbuf;
// The expose() event will be treated JUST ONCE at the first window creation
static gboolean justonce = TRUE;

/* Expect these functions */
void scan_jukebox(gpointer data);

/* Response to DELETE event (window destruction) */
static gint delete(GtkWidget *widget,
		   GtkWidget *event,
		   gpointer   data)
{
  if (jukebox_locked) {
    create_error_dialog(_("Cannot quit! Jukebox is busy!"));
    return TRUE;
  }
  write_prefs_file();
  jukebox_release();

  gtk_main_quit();
  return FALSE;
}

static GtkWidget *create_jukebox_menu(gchar **jukeboxes)
{
  GtkWidget *combo;
  GtkWidget *menu;
  GSList *group = NULL;
  GtkWidget *menu_item;
  gint i;

  /* Create the option menu */
#if GTK_CHECK_VERSION(2,24,0)
  // Use the new combo box type in 2.24 and later
  combo = gtk_combo_box_text_new();
#else
  combo = gtk_combo_box_new_text();
#endif

  /* Add a row to the menu */
  for (i = 0; i < vectorlength(jukeboxes); i++) {
#if GTK_CHECK_VERSION(2,24,0)
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), jukeboxes[i]);
#else
    gtk_combo_box_append_text(GTK_COMBO_BOX(combo), jukeboxes[i]);
#endif
  }

  gtk_combo_box_set_active(GTK_COMBO_BOX(combo), 0);

  return combo;
}

static void create_select_jukebox_dialog(void)
{
  gchar **jukeboxes;
  jukeboxes = jukebox_discover();
  if (jukeboxes != NULL) {
    if (vectorlength(jukeboxes) > 1) {
      gint i;
      GtkWidget *label;
      // Build a selection menu
      GtkWidget *select_jukebox_dialog;
      GtkWidget *jukebox_menu;
      GtkWidget *content_area;

      select_jukebox_dialog = gtk_message_dialog_new(GTK_WINDOW(main_window),
						     GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						     GTK_MESSAGE_QUESTION,
						     GTK_BUTTONS_OK_CANCEL,
						     _("Select jukebox"));



      jukebox_menu = create_jukebox_menu(jukeboxes);
      gtk_window_set_position (GTK_WINDOW(select_jukebox_dialog), GTK_WIN_POS_MOUSE);
      gtk_box_set_spacing (GTK_BOX(GTK_DIALOG(select_jukebox_dialog)->vbox), 5);

      label = gtk_label_new(_("Select jukebox to operate:"));

#if GTK_CHECK_VERSION(2,14,0)
      content_area = gtk_dialog_get_content_area(GTK_DIALOG(select_jukebox_dialog));
      gtk_container_add(GTK_CONTAINER(content_area), label);
      gtk_widget_show(label);
      gtk_container_add(GTK_CONTAINER(content_area), jukebox_menu);
      gtk_widget_show(jukebox_menu);
#else
      gtk_box_pack_start(GTK_BOX(GTK_DIALOG(select_jukebox_dialog)->vbox),
			 label, TRUE, TRUE, 0);
      gtk_widget_show(label);
      gtk_box_pack_start(GTK_BOX(GTK_DIALOG(select_jukebox_dialog)->vbox),
			 jukebox_menu, TRUE, TRUE, 0);
      gtk_widget_show(jukebox_menu);
#endif

      gtk_widget_show_all(select_jukebox_dialog);

      i = gtk_dialog_run(GTK_DIALOG(select_jukebox_dialog));
      if (i == GTK_RESPONSE_OK) {
	guint selection;

	selection = gtk_combo_box_get_active(GTK_COMBO_BOX(jukebox_menu));

	gtk_widget_destroy(select_jukebox_dialog);
	// Select the first jukebox
	if (jukebox_select(selection)) {
	  // Initially scan the jukebox
	  scan_jukebox(NULL);
	}
      } else {
	gtk_widget_destroy(select_jukebox_dialog);
      }
    } else {
      // Select the first jukebox
      if (jukebox_select(0)) {
	// Initially scan the jukebox
	scan_jukebox(NULL);
      }
    }
  }
}

/* Run at startup from the idle thread */
static gboolean startup_actions(gpointer p) {
  if (justonce) {
    justonce = FALSE;
    if (get_prefs_autoscan()) {
      // This will be treated JUST ONCE at the first window creation
      create_select_jukebox_dialog();
    }
    if (get_prefs_set_time()) {
      if (jukebox_selected() && !jukebox_locked) {
	jukebox_synchronize_time();
      }
    }
  }
  return FALSE;
}

/* The about box */
static void about_box(gpointer data)
{
  const gchar *authors[] = {"Linus Walleij - main code, parts of libnjb and libmtp",
			    "John Mechalas (Seagull) - libnjb",
			    "César Talón - Jukebox 3 examinations",
			    "Richard A. Low - initial MTP work and libmtp",
			    NULL};
  const gchar *copystring = "Copyright (C) 2011 Linus Walleij";
  gchar *aboutstring = _("A program to communicate with\nCreative Jukeboxes\nand MTP devices");
  gchar *translator_credits = _("translator_credits");

  GtkWidget *about;

  about = gtk_about_dialog_new();
  gtk_about_dialog_set_name(GTK_ABOUT_DIALOG(about), PACKAGE);
  gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(about), VERSION);
  gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(about), copystring);
  gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(about), aboutstring);
  gtk_about_dialog_set_license(GTK_ABOUT_DIALOG(about), "GPL 2+");
  gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(about), "http://gnomad2.sourceforge.net/");
  gtk_about_dialog_set_website_label(GTK_ABOUT_DIALOG(about), "Gnomad Homepage");
  gtk_about_dialog_set_authors(GTK_ABOUT_DIALOG(about), authors);
  if (strcmp(translator_credits, "translator_credits")) {
    gtk_about_dialog_set_translator_credits(GTK_ABOUT_DIALOG(about), translator_credits);
  }
  gtk_dialog_run(GTK_DIALOG(about));
  gtk_widget_destroy(about);
}

/*
 * Scan the jukebox for files
 */

void scan_jukebox(gpointer data)
{
  GtkWidget *label, *dialog;
  static scan_thread_arg_t scan_thread_args;

  if (!jukebox_selected()) {
    create_select_jukebox_dialog();
    return;
  }
  if (jukebox_locked) {
    create_error_dialog(_("Jukebox busy"));
    return;
  }
  jukebox_locked = TRUE;
  cancel_jukebox_operation = FALSE;

  dialog = gtk_message_dialog_new (GTK_WINDOW(main_window),
				   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				   GTK_MESSAGE_INFO,
				   GTK_BUTTONS_CLOSE,
				   _("Scanning jukebox library"));
  gtk_window_set_title (GTK_WINDOW (dialog), _("Retrieving metadata from jukebox"));
  g_signal_connect_object(GTK_OBJECT(dialog),
			  "delete_event",
			  G_CALLBACK(cancel_jukebox_operation_click),
			  NULL,
			  0);
  g_signal_connect_object(GTK_OBJECT(dialog),
			  "response",
			  G_CALLBACK(cancel_jukebox_operation_click),
			  NULL,
			  0);
  /* A blank label for progress messages */
  label = gtk_label_new("");
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), label, TRUE, TRUE, 0);
  gtk_widget_show_all(dialog);

  /* A bunch of arguments needed by the scanning thread */
  scan_thread_args.dialog = dialog;
  scan_thread_args.label = label;
  scan_thread_args.pltreestore = playlist_widgets.pltreestore;

  g_thread_create(scan_thread,(gpointer) &scan_thread_args, FALSE, NULL);
  return;
}

/* A dialog to select the owner */
static void set_jukeboxowner_dialog(gpointer data)
{
  GtkWidget *label;
  gchar *ownerstring;
  GtkWidget *jukeboxowner_dialog;
  GtkWidget *jukeboxowner_entry;
  gint i;
  gchar *owner;

  if (!jukebox_selected()) {
    create_select_jukebox_dialog();
    return;
  }
  jukeboxowner_dialog = gtk_message_dialog_new (GTK_WINDOW(main_window),
						GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_MESSAGE_QUESTION,
						GTK_BUTTONS_OK_CANCEL,
						_("Set owner string"));
  gtk_box_set_spacing (GTK_BOX(GTK_DIALOG(jukeboxowner_dialog)->vbox), 5);

  label = gtk_label_new(_("Edit the owner of this jukebox:"));
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(jukeboxowner_dialog)->vbox),
		     label, TRUE, TRUE, 0);
  gtk_widget_show(label);
  jukeboxowner_entry = gtk_entry_new();
  ownerstring = jukebox_get_ownerstring();
  if (ownerstring != NULL)
    gtk_entry_set_text(GTK_ENTRY(jukeboxowner_entry), ownerstring);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(jukeboxowner_dialog)->vbox), jukeboxowner_entry, TRUE, TRUE, 0);
  gtk_widget_show(jukeboxowner_entry);
  gtk_widget_show_all(jukeboxowner_dialog);
  i = gtk_dialog_run(GTK_DIALOG(jukeboxowner_dialog));
  if (i == GTK_RESPONSE_OK) {
    if (jukebox_locked) {
      create_error_dialog(_("Jukebox busy - change discarded"));
      return;
    }
    owner = g_strdup(gtk_entry_get_text(GTK_ENTRY(jukeboxowner_entry)));
    jukebox_set_ownerstring(owner);
    g_free(owner);
    gtk_widget_destroy(jukeboxowner_dialog);
  } else {
    gtk_widget_destroy(jukeboxowner_dialog);
  }
}


static void information_dialog(gpointer data)
{
  GtkWidget *dialog;

  if (jukebox_locked) {
    create_error_dialog(_("Cannot view information! Jukebox is busy!"));
    return;
  }

  dialog = jukebox_get_deviceinfo_dialog();
  if (dialog != NULL) {
    gtk_widget_show(dialog);
  } else {
    create_error_dialog(_("Cannot retrieve jukebox information!"));
  }
}

#ifdef HAVE_GUDEV
static guint
get_property_as_int(GUdevDevice *device, const char *property, int base)
{
        const char *strvalue;

        strvalue = g_udev_device_get_property(device, property);
        if (strvalue == NULL) {
                return 0;
        }
        return strtol(strvalue, NULL, base);
}

static void uevent_cb(GUdevClient *client, const char *action, GUdevDevice *device, void *data)
{
  guint64 devicenum;
  guint vendor;
  guint model;
  guint busnum;
  guint devnum;
  guint mtpdevice;
  guint njbdevice;

  devicenum = (guint64) g_udev_device_get_device_number(device);
  g_print("%s event for %s (%"G_GINT64_MODIFIER"x)", action,
	  g_udev_device_get_sysfs_path (device), devicenum);

  /* get device info */
  vendor = get_property_as_int(device, "ID_VENDOR_ID", 16);
  model = get_property_as_int(device, "ID_MODEL_ID", 16);
  busnum = get_property_as_int(device, "BUSNUM", 10);
  devnum = get_property_as_int(device, "DEVNUM", 10);
  mtpdevice = get_property_as_int(device, "ID_MTP_DEVICE", 10);
  njbdevice = get_property_as_int(device, "ID_LIBNJB_DEVICE", 10);

  if (vendor == 0 || model == 0) {
    g_print("couldn't get vendor or model ID for device at (%x:%x)\n",
	    busnum, devnum);
    return;
  } else {
    g_print("vendor = %x, model = %x, bus = %x, device = %x\n",
	    vendor, model, busnum, devnum);
  }

  if (mtpdevice || njbdevice) {
    g_print("device is MTP or libnjb compliant\n");

    if (g_str_equal(action, "add")) {
      // If we haven't yet selected a jukebox, take this one!
      if (uevent_bus_hooked == 0 &&
	  uevent_device_hooked == 0 &&
	  !jukebox_selected()) {
	g_print("Unhooked, hook this device!\n");
	uevent_bus_hooked = busnum;
	uevent_device_hooked = devnum;
	scan_jukebox(NULL);
      }
    } else if (g_str_equal (action, "remove")) {
      if (uevent_bus_hooked == busnum &&
	  uevent_device_hooked == devnum) {
	g_print("Found hooked device, unhook this device!\n");
	jukebox_release();
	uevent_bus_hooked = 0;
	uevent_device_hooked = 0;
      }
    }
  }
}
#endif

/*
 * And this is where everything starts, everything graphic is initialized etc
 */
int main(int argc,
	 char *argv[])
{
  const gchar cProgramName[]= PACKAGE " " VERSION;
  /* For the menus */
  GtkWidget *menu;
  GtkWidget *menu_bar;
  GtkWidget *root_menu;
  GtkWidget *menu_item;
  GtkWidget *music_panel;
  GtkWidget *data_panel;

  GtkWidget *vbox, *prefshbox;
  // GtkWidget *hbox;
  GtkWidget *notebook;
  GtkWidget *label;
  GtkAccelGroup *accel_group;
  extern char *optarg;
  gint opt;
  GError *icon_load_error = NULL;

  /* Binds for internationalization (i18n) */
  bindtextdomain (GETTEXT_PACKAGE, GNOMADLOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  g_thread_init(NULL);

  gtk_init(&argc, &argv);
  /* Parse extra arguments */
  gnomad_debug = 0;
  while ((opt = getopt(argc, argv, "D:")) != -1 ) {
    switch (opt) {
    case 'D':
      gnomad_debug = atoi(optarg);
      break;
    default:
      break;
    }
  }

  main_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (main_window), cProgramName);
  icon_pixbuf = gdk_pixbuf_new_from_file(PIXMAPSDIR "/gnomad2-logo.png", &icon_load_error);
  if (icon_pixbuf != NULL) {
    gtk_window_set_default_icon(icon_pixbuf);
  }
  gtk_window_set_wmclass(GTK_WINDOW(main_window), PACKAGE, "main");
  gtk_widget_set_usize(GTK_WIDGET(main_window), 640, 480);

  g_signal_connect(GTK_OBJECT(main_window),
		   "delete_event",
		   G_CALLBACK(delete),
		   NULL);

  gtk_container_set_border_width (GTK_CONTAINER (main_window), 0);

  /* Create metadata list stores */
  create_list_stores();

  /* Create a GtkAccelGroup and add it to the window. */
  accel_group = gtk_accel_group_new();
  gtk_window_add_accel_group (GTK_WINDOW (main_window), accel_group);

  /* Stuff everything into a vertical box */
  vbox = gtk_vbox_new(FALSE, 5);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 0);
  gtk_container_add(GTK_CONTAINER(main_window), vbox);
  gtk_widget_show(vbox);

  /* Create the menu bar */
  menu_bar = gtk_menu_bar_new ();
  gtk_widget_show (menu_bar);
  gtk_box_pack_start (GTK_BOX (vbox), menu_bar, FALSE, FALSE, 0);

  menu = gtk_menu_new (); // Shall not be shown!

  menu_item = gtk_menu_item_new_with_label(_("Quit"));
  gtk_menu_shell_append (GTK_MENU_SHELL(menu), menu_item);
  gtk_widget_add_accelerator (menu_item, "activate", accel_group,
                              GDK_q, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
  g_signal_connect(GTK_OBJECT(menu_item),
		   "activate",
		   G_CALLBACK(delete),
		   NULL);
  gtk_widget_show(menu_item);
  root_menu = gtk_menu_item_new_with_label(_("File"));
  gtk_widget_show(root_menu);
  gtk_menu_item_set_submenu(GTK_MENU_ITEM (root_menu), menu);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), root_menu);

  /* Create a new notebook, place the position of the tabs */
  notebook = gtk_notebook_new();
  gtk_notebook_set_tab_pos(GTK_NOTEBOOK(notebook), GTK_POS_TOP);
  gtk_box_pack_start(GTK_BOX(vbox), notebook, TRUE, TRUE, 0);
  gtk_widget_show(notebook);

  /*
   * Prefs tab
   */
  prefshbox = gtk_hbox_new(FALSE, 0);
  gtk_widget_show (prefshbox);
  /* Put preferences in the hbox */
  create_prefs_widgets(prefshbox);
  // gtk_box_pack_start(GTK_BOX(hbox), transfer_widgets.playlistmenu, TRUE, TRUE, 0);

  /*
   * Music transfer tab
   */
  music_panel = create_xfer_widgets();
  label = gtk_label_new(_("Music transfer"));
  gtk_notebook_append_page (GTK_NOTEBOOK(notebook), music_panel, label);

  /*
   * Playlists tab
   * This is an option menu wih playlist that is filled in
   * by the scanning function
   */
  vbox=gtk_vbox_new(FALSE, 0);
  create_playlist_widgets(vbox);
  gtk_widget_show(vbox);
  label = gtk_label_new (_("Playlists"));
  gtk_notebook_append_page (GTK_NOTEBOOK(notebook), vbox, label);

  /*
   * Data file transfer tab
   */
  data_panel = create_data_widgets();
  label = gtk_label_new (_("Data transfer"));
  gtk_notebook_append_page (GTK_NOTEBOOK(notebook), data_panel, label);

  /* Add prefs tab */
  label = gtk_label_new (_("Preferences"));
  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), prefshbox, label);

  /* Set what page to start at (page 4) */
  gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), 0);

  menu = gtk_menu_new (); // Shall not be shown!
  menu_item = gtk_menu_item_new_with_label (_("Rescan contents"));
  gtk_menu_shell_append (GTK_MENU_SHELL(menu), menu_item);
  gtk_widget_add_accelerator (menu_item, "activate", accel_group,
                              GDK_r, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
  g_signal_connect(GTK_OBJECT (menu_item),
		   "activate",
		   G_CALLBACK(scan_jukebox),
		   NULL);
  gtk_widget_show(menu_item);
  menu_item = gtk_menu_item_new_with_label(_("Set owner string"));
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);
  g_signal_connect(GTK_OBJECT (menu_item),
		   "activate",
		   G_CALLBACK(set_jukeboxowner_dialog),
		   NULL);
  gtk_widget_show (menu_item);
  menu_item = gtk_menu_item_new_with_label (_("Jukebox information"));
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);
  gtk_widget_add_accelerator (menu_item, "activate", accel_group,
                              GDK_i, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
  g_signal_connect(GTK_OBJECT(menu_item),
		   "activate",
		   G_CALLBACK(information_dialog),
		   NULL);
  gtk_widget_show (menu_item);
  root_menu = gtk_menu_item_new_with_label (_("Jukebox library"));
  gtk_widget_show (root_menu);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (root_menu), menu);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), root_menu);

  menu = gtk_menu_new(); // Shall not be shown!
  menu_item = gtk_menu_item_new_with_label (_("About"));
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);
  g_signal_connect(GTK_OBJECT (menu_item),
		   "activate",
		   G_CALLBACK(about_box),
		   NULL);
  gtk_widget_show (menu_item);
  root_menu = gtk_menu_item_new_with_label (_("Help"));
  gtk_widget_show (root_menu);
  gtk_menu_item_set_right_justified(GTK_MENU_ITEM(root_menu), TRUE);
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(root_menu), menu);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), root_menu);

#ifdef HAVE_GUDEV
  /*
   * Monitor udev device events - we're only really interested in events
   * for USB devices.
   */
  gudev_client = g_udev_client_new(gudev_subsystems);
  uevent_id = g_signal_connect_object(gudev_client,
				      "uevent",
				      G_CALLBACK(uevent_cb),
				      NULL, 0);
#endif

  gtk_widget_show(main_window);
  g_idle_add(startup_actions, NULL);
  gtk_main();
  return 0;
}
