/* prefs.h
   Graphical widgets and callbacks for the preferences frame
   header file
   Copyright (C) 2001 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA. 

*/

#ifndef PREFSH_INCLUDED
#define PREFSH_INCLUDED 1

/* Get windows indicator macro */
#include <gtk/gtk.h>

/* Exported functions */
void write_prefs_file(void);
gboolean get_prefs_tagoverride(void);
gboolean get_prefs_tagremove(void);
gboolean get_prefs_usetag(void);
gboolean get_prefs_useid3_unicode(void);
gboolean get_prefs_display_dotdirs(void);
gboolean get_prefs_ask_playlist(void);
gboolean get_prefs_recurse_dir(void);
#ifndef G_OS_WIN32
gboolean get_prefs_force_8859(void);
#endif
gboolean get_prefs_detect_metadata(void);
gboolean get_prefs_extended_metadata(void);
gboolean get_prefs_use_origname(void);
gboolean get_prefs_autoscan(void);
gboolean get_prefs_set_time(void);
gboolean get_prefs_turbo_mode(void);
gchar *get_prefs_filenameformat(void);
void create_prefs_widgets(GtkWidget *box);

#endif
