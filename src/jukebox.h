/* jukebox.h
   Functions related to the communication with jukebox hardware
   using the libnjb library, header file
   Copyright (C) 2001-2007 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA. 

*/

#ifndef JUKEBOXH_INCLUDED
#define JUKEBOXH_INCLUDED 1

/*
 * The jukebox library is included into all files
 * using this file
 */
#include <libnjb.h>

/*
 * libmtp is included conditionally.
 */
#ifdef HAVE_LIBMTP
#include <libmtp.h>
#endif

#include "metadata.h"

typedef struct 
{
  GtkWidget *dialog;
  GtkWidget *label;
  GtkTreeStore *pltreestore;
} scan_thread_arg_t;

typedef struct 
{
  GtkWidget *dialog;
  GtkWidget *label;
  GList *metalist;
} jb2hd_thread_arg_t;

typedef struct 
{
  GtkWidget *dialog;
  GtkWidget *label;
  GList *metalist;
} jb2hd_data_thread_arg_t;

typedef struct 
{
  GtkWidget *dialog;
  GtkWidget *label;
  GtkTreeStore *pltreestore;
  GList *metalist;
  GList *playlists;
} hd2jb_thread_arg_t;

typedef struct 
{
  GtkWidget *dialog;
  GtkWidget *label;
  GList *metalist;
} hd2jb_data_thread_arg_t;

typedef struct 
{
  GtkWidget *dialog;
  GtkWidget *songlabel;
  GtkWidget *timelabel;
  GtkAdjustment *adj;
  GList *metalist;
} play_thread_arg_t;

typedef struct 
{
  gchar *name;
  gchar *plid;
  GSList *tracklist;
} gnomadplaylist_entry_t;

/* Exported functions */
gchar **jukebox_discover(void);
gboolean jukebox_select(gint i);
gboolean jukebox_selected(void);
gboolean jukebox_is_mtp(void);
GtkWidget *jukebox_get_deviceinfo_dialog(void);
void cancel_jukebox_operation_click(GtkButton *button, gpointer data);
void jukebox_release(void);
void rebuild_datafile_list(gchar *filter);
gpointer scan_thread(gpointer thread_args);
gpointer jb2hd_thread(gpointer thread_args);
gpointer jb2hd_data_thread(gpointer thread_args);
gpointer hd2jb_thread(gpointer thread_args);
gpointer hd2jb_data_thread(gpointer thread_args);
void jukebox_delete_tracks(GList *metalist, GtkTreeStore *pltreestore);
void jukebox_delete_files(GList *metalist);
gboolean jukebox_begin_metadata_set(void);
void jukebox_set_metadata (metadata_t *meta);
void jukebox_end_metadata_set(void);
guint32 jukebox_create_playlist(gchar *plname, GtkTreeStore *pltreestore);
void jukebox_delete_playlist(guint32 plid);
void jukebox_rename_playlist(guint32 plid, gchar *name, 
			     GtkTreeStore *pltreestore);
void add_tracks_to_playlists(GList *playlists, GList *metalist);
void build_playlist_tree(void);
guint jukebox_randomize_playlist(guint playlist, GtkTreeStore *pltreestore);
guint jukebox_delete_track_from_playlist(guint32 trackid, guint32 plist, GtkTreeStore *pltreestore);
gchar *jukebox_get_ownerstring(void);
void jukebox_set_ownerstring(gchar *owner);
gchar *jukebox_get_time(void);
void jukebox_reset_get_eax(void);
njb_eax_t *jukebox_get_eax(void);
void jukebox_destroy_eax(njb_eax_t *eax);
void jukebox_adjust_eax(guint16 effect, guint16 patch, gint16 value);
GList *jukebox_get_playlist_for_play(guint plid);
metadata_t *jukebox_get_current_playing_metadata(void);
void jukebox_skip_songposition(guint songpos);
void jukebox_previous(void);
void jukebox_next(gboolean already_playing);
gpointer play_thread(gpointer thread_args);
void jukebox_synchronize_time(void);
void jukebox_create_folder(gchar *foldername);

#endif
