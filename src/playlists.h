/* playlists.h
   Playlist GUI and call handling, header file
   Copyright (C) 2001 Linus Walleij

This file is part of the GNOMAD package.

GNOMAD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

You should have received a copy of the GNU General Public License
along with GNOMAD; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA. 

*/

#ifndef PLAYLISTSH_INCLUDED
#define PLAYLISTSH_INCLUDED 1

/* Exported functions */
void jbmenu_add_playlist_response(gpointer data);
void transfer_from_hd_dialog (GtkButton *inbutton, gpointer data);
void create_playlist_widgets(GtkWidget *box);

#endif
